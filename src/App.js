import './App.css';
import Intent from './Intent';
import 'bootstrap/dist/css/bootstrap.css';
import { useState } from 'react';

// value to be pulled on an API call
const IntentData = [{"Child":[{"ID":1,"RefID":6,"Value":"Purchase (CO)"},{"ID":2,"RefID":7,"Value":"Purchase (DD)"},{"ID":3,"RefID":8,"Value":"Telegraphic Transfer"}],"Code":"CHE","ID":1,"RefID":1,"Value":"Cheque / CO / DD / TT"},{"Child":[{"ID":1,"RefID":9,"Value":"Cash(SGD)"},{"ID":2,"RefID":10,"Value":"Cash (FCY)"},{"ID":3,"RefID":11,"Value":"Cash Cheque (SGD)"},{"ID":4,"RefID":12,"Value":"Cash Cheque (FCY)"}],"Code":"DEP","ID":2,"RefID":2,"Value":"Deposit"},{"Child":[{"ID":1,"RefID":13,"Value":"Placement (SGD)"},{"ID":2,"RefID":14,"Value":"Placement (FCY)"},{"ID":3,"RefID":15,"Value":"Renewal"}],"Code":"FIX","ID":3,"RefID":3,"Value":"Fix Deposit"},{"Child":[{"ID":1,"RefID":16,"Value":"Loan Drawdown (SGD)"},{"ID":2,"RefID":17,"Value":"Loan Drawdown (FCY)"},{"ID":3,"RefID":18,"Value":"Full Early Repayment"},{"ID":4,"RefID":19,"Value":"Capital Repayment"},{"ID":5,"RefID":20,"Value":"Partial Early Repayment"},{"ID":6,"RefID":21,"Value":"Repayment (CASA)"},{"ID":7,"RefID":22,"Value":"Repayment (CASH)"},{"ID":8,"RefID":23,"Value":"Repayment (Cheque)"},{"ID":9,"RefID":24,"Value":"Repayment (FCY)"}],"Code":"LNS","ID":4,"RefID":4,"Value":"Loans and Payment"},{"Child":[{"ID":1,"RefID":9,"Value":"Cash(SGD)"},{"ID":2,"RefID":10,"Value":"Cash (FCY)"},{"ID":3,"RefID":11,"Value":"Cash Cheque (SGD)"},{"ID":4,"RefID":12,"Value":"Cash Cheque (FCY)"},{"ID":5,"RefID":25,"Value":"Cash Advance"}],"Code":"WDL","ID":5,"RefID":5,"Value":"Withdrawal"}]

// value to be pulled on an API call
const LangData = [{"Code":"EN","Data":[{"RefID":1,"value":"Cheque / CO / DD / TT"},{"RefID":2,"value":"Deposit"},{"RefID":2,"value":"Fixed Deposit"},{"RefID":4,"value":"Loans and Payment"},{"RefID":5,"value":"Withdrawal"},{"RefID":6,"value":"Purchase (CO)"},{"RefID":7,"value":"Purchase (DD)"},{"RefID":8,"value":"Telegraphic Transfer"},{"RefID":9,"value":"Cash(SGD)"},{"RefID":10,"value":"Cash (FCY)"},{"RefID":11,"value":"Cash Cheque (SGD)"},{"RefID":12,"value":"Cash Cheque (FCY)"},{"RefID":13,"value":"Placement (SGD)"},{"RefID":14,"value":"Placement (FCY)"},{"RefID":15,"value":"Renewal"},{"RefID":16,"value":"Loan Drawdown (SGD"},{"RefID":17,"value":"Loan Drawdown (FCY)"},{"RefID":18,"value":"Full Early Repayment"},{"RefID":19,"value":"Capital Repayment"},{"RefID":20,"value":"Partial Early Repayment"},{"RefID":21,"value":"Repayment (CASA)"},{"RefID":22,"value":"Repayment (CASH)"},{"RefID":23,"value":"Repayment (Cheque)"},{"RefID":24,"value":"Repayment (FCY)"},{"RefID":25,"value":"Cash Advance"}],"Description":"English"},{"Code":"CN","Data":[{"RefID":1,"value":"查看 / CO / DD / TT"},{"RefID":2,"value":"订金"},{"RefID":2,"value":"定期存款"},{"RefID":4,"value":"贷款和付款"},{"RefID":5,"value":"退出"},{"RefID":6,"value":"购买 (CO)"},{"RefID":7,"value":"购买 (DD)"},{"RefID":8,"value":"Telegraphic Transfer"},{"RefID":9,"value":"现金(SGD)"},{"RefID":10,"value":"现金 (FCY)"},{"RefID":11,"value":"现金支票 (SGD)"},{"RefID":12,"value":"现金支票 (FCY)"},{"RefID":13,"value":"放置 (SGD)"},{"RefID":14,"value":"放置 (FCY)"},{"RefID":15,"value":"Renewal"},{"RefID":16,"value":"续订 (SGD"},{"RefID":17,"value":"续订 (FCY)"},{"RefID":18,"value":"全额提前还款"},{"RefID":19,"value":"Capital Repayment"},{"RefID":20,"value":"资本偿还"},{"RefID":21,"value":"还款 (CASA)"},{"RefID":22,"value":"还款 (CASH)"},{"RefID":23,"value":"还款 (Cheque)"},{"RefID":24,"value":"还款 (FCY)"},{"RefID":25,"value":"预借现金"}],"Description":"Chinese"},{"Code":"MY","Data":[{"RefID":1,"value":"Semak / CO / DD / TT"},{"RefID":2,"value":"Deposit"},{"RefID":2,"value":"Deposit tetap"},{"RefID":4,"value":"Pinjaman dan Pembayaran"},{"RefID":5,"value":"Mengeluarkan"},{"RefID":6,"value":"Belian (CO)"},{"RefID":7,"value":"Belian (DD)"},{"RefID":8,"value":"Pemindahan Telegraf"},{"RefID":9,"value":"Tunai(SGD)"},{"RefID":10,"value":"Tunai (FCY)"},{"RefID":11,"value":"Tunai Semak (SGD)"},{"RefID":12,"value":"Tunai Semak (FCY)"},{"RefID":13,"value":"Penempatan (SGD)"},{"RefID":14,"value":"Penempatan (FCY)"},{"RefID":15,"value":"Pembaharuan"},{"RefID":16,"value":"Pengeluaran Pinjaman (SGD"},{"RefID":17,"value":"Pengeluaran Pinjaman (FCY)"},{"RefID":18,"value":"Bayaran Balik Awal Penuh"},{"RefID":19,"value":"Capital Repayment"},{"RefID":20,"value":"Bayaran Balik Modal"},{"RefID":21,"value":"Bayaran balik (CASA)"},{"RefID":22,"value":"Bayaran balik (CASH)"},{"RefID":23,"value":"Bayaran balik (Cheque)"},{"RefID":24,"value":"Bayaran balik (FCY)"},{"RefID":25,"value":"Pendahuluan tunai"}],"Description":"Malay"}]

function App() {
    const [LangInd, setLangInd] = useState(0)

    const changeLang = (e) => {
      /** mapping is simplified on just for this demo */
      setLangInd(parseInt(e.target.id)) 
    }
  return (
    <div className="">
        <div className='Lang-container'>
          <button id='0' onClick={changeLang} className='btn btn-link'>English</button> | 
          <button id='1' onClick={changeLang} className='btn btn-link'>中国人</button> | 
          <button id='2' onClick={changeLang} className='btn btn-link'>Melayu</button>
        </div>
        <Intent IntentData={IntentData} LangData={LangData[LangInd]}></Intent>
    </div>
  );
}

export default App;
