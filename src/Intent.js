import React, { useEffect, useState } from 'react'
import SubIntent from './SubIntent'
const Intent = (props) => {
    const [ChildIntent, setChildIntent] = useState([])
    const onClick=(e)=>{
        setChildIntent(props.IntentData[e.target.id].Child)
    }
  return (
    
    <div>
        <div className='row'>
            <div className='col-4 main-intent-container'>
                {
                    props.IntentData.map((Main, index)=>(
                        props.LangData.Data.map((lang, idx)=> Main.RefID===lang.RefID ? ( 
                            <div key={idx} id={idx} onClick={onClick} className='btn-main mb-2'>{lang.value}</div>
                        ): '') 
                    ))
                }
            </div>
            <div className='col-8'>
                <SubIntent ChildIntent={ChildIntent} LangData={props.LangData}></SubIntent>
            </div>
        </div>
    </div>
  )
}

export default Intent