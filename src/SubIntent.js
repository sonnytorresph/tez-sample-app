import React from 'react'

const SubIntent = (props) => {
  return (
    <div className='row childIntent-container'>
        {props.ChildIntent.length > 0 ? props.ChildIntent.map((child, index)=> (
            props.LangData.Data.map((lang, idx)=> child.RefID===lang.RefID ? ( 
                <div key={index} className="col-3 child-intent-btn"><div className='child-intent-label'>{lang.value}</div></div>
            ): '') 
        ))
        : 'No intent to display' }
    </div>
  )
}

export default SubIntent